
state={
	id=343
	name="STATE_343" # Afyon
	manpower = 477880
	resources={
		# chromium=25 # was: 32
	}
	
	state_category = rural

	history={
		owner = GRE
		# Greece as Occupant
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		add_core_of = TUR
	}

	provinces={
		3856 3983 6945 11831
	}
}
