capital = 137

set_politics = {
    ruling_party = interventionalism
    last_election = "1947.1.1"
    election_frequency = 48
    elections_allowed = no
}

set_popularities = {
    imperialism = 0
    internationalism = 25
    interventionalism = 50
    isolationism = 25
}