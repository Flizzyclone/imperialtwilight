
ideologies = {

	imperialism = {
		types = {
			imperialism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_IMPERIALISM_1"
			"FACTION_NAME_IMPERIALISM_2"
			"FACTION_NAME_IMPERIALISM_3"
			"FACTION_NAME_IMPERIALISM_4"
			"FACTION_NAME_IMPERIALISM_5"
			"FACTION_NAME_IMPERIALISM_6"
		}
		color = { 189 53 53 }
		rules = {
			can_force_government = no
			can_puppet = no
			can_join_factions = no
			can_send_volunteers = yes
			can_create_factions = no
		}
		modifiers = {
			join_faction_tension = 0.80
			generate_wargoal_tension = 0.25
			conscription = 0.005
		}
		ai_neutral = yes	
		can_be_boosted = no
	}
	internationalism = {
		types = {
			internationalism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_INTERNATIONALISM_1"
			"FACTION_NAME_INTERNATIONALISM_2"
			"FACTION_NAME_INTERNATIONALISM_3"
			"FACTION_NAME_INTERNATIONALISM_4"
			"FACTION_NAME_INTERNATIONALISM_5"
			"FACTION_NAME_INTERNATIONALISM_6"
		}
		color = { 66 74 189 }
		rules = {
			can_force_government = no
			can_puppet = no
			can_join_factions = no
			can_send_volunteers = yes
			can_create_factions = no
		}
		modifiers = {
			join_faction_tension = 0.80
			generate_wargoal_tension = 0.25
			conscription = 0.005
		}
		ai_neutral = yes	
		can_be_boosted = no
	}
	interventionalism = {
		types = {
			interventionalism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_INTERVENTIONALISM_1"
			"FACTION_NAME_INTERVENTIONALISM_2"
			"FACTION_NAME_INTERVENTIONALISM_3"
			"FACTION_NAME_INTERVENTIONALISM_4"
			"FACTION_NAME_INTERVENTIONALISM_5"
			"FACTION_NAME_INTERVENTIONALISM_6"
		}
		color = { 228 194 75 }
		rules = {
			can_force_government = no
			can_puppet = no
			can_join_factions = no
			can_send_volunteers = yes
			can_create_factions = no
		}
		modifiers = {
			join_faction_tension = 0.80
			generate_wargoal_tension = 0.25
			conscription = 0.005
		}
		ai_neutral = yes	
		can_be_boosted = no
	}
	isolationism = {
		types = {
			isolationism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_ISOLATIONISM_1"
			"FACTION_NAME_ISOLATIONISM_2"
			"FACTION_NAME_ISOLATIONISM_3"
			"FACTION_NAME_ISOLATIONISM_4"
			"FACTION_NAME_ISOLATIONISM_5"
			"FACTION_NAME_ISOLATIONISM_6"
		}
		color = { 49 189 85 }
		rules = {
			can_force_government = no
			can_puppet = no
			can_join_factions = no
			can_send_volunteers = yes
			can_create_factions = no
		}
		modifiers = {
			join_faction_tension = 0.80
			generate_wargoal_tension = 0.25
			conscription = 0.005
		}
		ai_neutral = yes	
		can_be_boosted = no
	}
}
	
